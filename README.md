## Thanking
First of all, thank for the opportunity to improves my skills about tailwind, nuxt and course, vueJS, above i describe some details about runing and project, i hope your like that.

## Install and Running
```bash
git clone https://gitlab.com/lpj145/dps-test

cd dps-test

npm install

npm run dev
```
This live :), go to: `http://localhost:3000`

### Details about project
I love **simple** word, because everything is simple, is simple to maintain, to tech, to learn, and this is reason why i can't adopt plugins or abstraction to do this test.
This make me don't add vuex, store or any other store solutions, so just used ``data>>props>>event``.

Components from ui are specified on `components/Ui`

Components from Office cards and the reason of project are
specified on `components/`

I'm not add tests, because this increase time of job and for some unknow reason, this is first test don't talk about tests, but, it's ok, don't have any sense to test zero integrations components i think ( exception like you making giant UI library. )

![Dog and Pony Studios](https://www.dogandponystudios.com/app/themes/dps/assets/public/images/logo-fbe89868bd.svg)

# Senior Frontend Developer Test

**Welcome to our Senior Frontend Developer Test**

This test consists of a series of tasks which may take between 2–4 hours to complete, depending on your experience level. The primary purpose of this test is to infer your “logical thinking” and “problem-solving” skills.


## Scenario

To recreate the design and functionality of a generic office-listings page. Please navigate through the [interactive prototype](https://www.figma.com/proto/VU2BJHrMmoSEdQmMa1EbYP/Front-end-Test?node-id=451%3A336&viewport=767%2C416%2C0.5802898406982422&scaling=min-zoom) to better understand the scope and requirements.


## Tasks

1. Fork this repository (to jump-start your test) [Done]
1. Recreate [the design](https://www.figma.com/proto/VU2BJHrMmoSEdQmMa1EbYP/Front-end-Test?node-id=451%3A336&viewport=767%2C416%2C0.5802898406982422&scaling=min-zoom) using [Vue.js](https://vuejs.org/) and [TailwindCSS](https://tailwindcss.com/) [Done]
1. Populate the office listings from a datasource (JSON or hard-coded array) [Done]
1. Recreate the following interactivity [Done]
    1. Toggle open/close office cards [Done]
    1. “Add new location” (add new office to list) [Done]
    1. “Edit office” (edit and save office data) [Done]
    1. While adding or editing an office, the “Save” button should be disabled until all fields have been validated [Done]
    1. “Delete office” (remove office from list) [Done]


## Deadline

Please submit your test as a Github repository URL, along with a readme file containing instructions on how to set up and run your application. The deadline for submissions is **Monday, 12th of October**. _Submissions will not be accepted after this date._


## Tips

- Keep it simple
- Data persistence is not required
- Please ensure your readme file contains valid and simple instructions
- Please use placeholder data only (i.e. do not use real or identifiable places)
- Feel free to validate fields according to your own validation logic
- Verify your application fully functions prior to submission


## Submissions

[Submit your test here](https://forms.gle/UcqQkBqCCvZhi7pe8)
