export { default as OfficeCard } from '../..\\components\\OfficeCard.vue'
export { default as OfficeForm } from '../..\\components\\OfficeForm.vue'
export { default as OfficeFormSection } from '../..\\components\\OfficeFormSection.vue'
export { default as TheTopAlert } from '../..\\components\\TheTopAlert.vue'
export { default as VSmoothReflow } from '../..\\components\\VSmoothReflow.vue'
export { default as VBtn } from '../..\\components\\Ui\\VBtn.vue'
export { default as VCard } from '../..\\components\\Ui\\VCard.vue'
export { default as VColorInput } from '../..\\components\\Ui\\Inputs\\VColorInput.vue'
export { default as VInputText } from '../..\\components\\Ui\\Inputs\\VInputText.vue'

export const LazyOfficeCard = import('../..\\components\\OfficeCard.vue' /* webpackChunkName: "components_OfficeCard" */).then(c => c.default || c)
export const LazyOfficeForm = import('../..\\components\\OfficeForm.vue' /* webpackChunkName: "components_OfficeForm" */).then(c => c.default || c)
export const LazyOfficeFormSection = import('../..\\components\\OfficeFormSection.vue' /* webpackChunkName: "components_OfficeFormSection" */).then(c => c.default || c)
export const LazyTheTopAlert = import('../..\\components\\TheTopAlert.vue' /* webpackChunkName: "components_TheTopAlert" */).then(c => c.default || c)
export const LazyVSmoothReflow = import('../..\\components\\VSmoothReflow.vue' /* webpackChunkName: "components_VSmoothReflow" */).then(c => c.default || c)
export const LazyVBtn = import('../..\\components\\Ui\\VBtn.vue' /* webpackChunkName: "components_Ui/VBtn" */).then(c => c.default || c)
export const LazyVCard = import('../..\\components\\Ui\\VCard.vue' /* webpackChunkName: "components_Ui/VCard" */).then(c => c.default || c)
export const LazyVColorInput = import('../..\\components\\Ui\\Inputs\\VColorInput.vue' /* webpackChunkName: "components_Ui/Inputs/VColorInput" */).then(c => c.default || c)
export const LazyVInputText = import('../..\\components\\Ui\\Inputs\\VInputText.vue' /* webpackChunkName: "components_Ui/Inputs/VInputText" */).then(c => c.default || c)
