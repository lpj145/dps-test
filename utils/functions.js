export const randomId = (prefix = '') => `${prefix}-${Math.round((Math.random() * 36 ** 12)).toString(36)}`

/**
 * @param {Array} fields
 */
export const initializeData = (fields, valid = false) => {
  return fields.reduce((opts, field) => {
    opts[field] = { valid, value: '' }

    return opts
  }, {})
}

/**
 * @param {Number} max
 */
export const randomNumber = max => Math.floor(Math.random() * max)

export const isString = v => typeof v === 'string'

export const removeFrom = (object, array) => {
  return array.filter(item => item !== object)
}
