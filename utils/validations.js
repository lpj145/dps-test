import { isString } from '~/utils/functions'

export const minLength = (v, minLength) => {
  if (isString(v) && v.length >= minLength) {
    return true
  }

  return `need equal or more than ${minLength} characters.`
}

export const maxLength = (v, maxLength) => {
  if (isString(v) && v.length <= maxLength) {
    return true
  }

  return `need equal or less than ${maxLength} characters.`
}

export const isPhoneNumber = (phone, phoneLength = 15) => {
  if (minLength(phone, phoneLength) === true && maxLength(phone, phoneLength) === true) {
    return true
  }

  return 'Invalid phone number.'
}

export const isEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (re.test(String(email).toLowerCase())) {
    return true
  }

  return 'Invalid email.'
}
